# Makefile used after a clean install; faster setup!

install_homebrew:
	@echo "Installing Homebrew..." 
	/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	@echo "Homebrew installed!" 
	touch $(HOME)/brew_installed

install_packages:
	brew install neofetch
	brew install tree

install_apps:
	brew cask install pycharm
	brew cask install intellij-idea
	brew cask install lastpass
	brew cask install iterm2
	brew cask install postman
	brew cask install google-backup-and-sync
	brew cask install google-chrome
	brew cask install docker

install_scala: $(HOME)/brew_installed
	brew tap adoptopenjdk/openjdk 
	brew cask install adoptopenjdk8 
	brew install scala

install_zsh:
	sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended 
	brew install zsh-syntax-highlighting 
	brew install zsh-autosuggestions 
	@echo "\n\nsource /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" >> $(HOME)/.zshrc